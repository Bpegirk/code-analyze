﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;

namespace SortDiff
{
    class Program
    {

        private static List<int> _arr;
        static void Main(string[] args)
        {
            const int len = 10000;

            Console.WriteLine("Fill array of " + len + " random int elements");
            FillArray(len);

            List<int> result = new List<int>();

            Console.WriteLine("BubbleSort start");
            result = BubbleSort();

            Console.WriteLine("InsertionSort start");
            result = InsertionSort();

            Console.WriteLine("SelectionSort start");
            result = SelectionSort();


        }

        static void FillArray(int len)
        {
            _arr = new List<int>();
            Random rnd = new Random();
            for (int i = 0; i < len; i++)
            {
                _arr.Add(rnd.Next(int.MinValue, int.MaxValue));
            }
        }

        static List<int> BubbleSort()
        {
            List<int> sortedArr = new List<int>(_arr);
            bool itemMoved = false;
            do
            {
                itemMoved = false;
                for (int i = 0; i < sortedArr.Count - 1; i++)
                {
                    if (sortedArr[i] > sortedArr[i + 1])
                    {
                        int lowerValue = sortedArr[i + 1];
                        sortedArr[i + 1] = sortedArr[i];
                        sortedArr[i] = lowerValue;
                        itemMoved = true;
                    }
                }
            } while (itemMoved);

            return sortedArr;
        }

        static List<int> InsertionSort()
        {
            List<int> sortedArr = new List<int>(_arr);

            for (int i = 0; i < sortedArr.Count; i++)
            {
                int item = sortedArr[i];
                int currentIndex = i;

                while (currentIndex > 0 && sortedArr[currentIndex - 1] > item)
                {
                    sortedArr[currentIndex] = sortedArr[currentIndex - 1];
                    currentIndex--;
                }

                sortedArr[currentIndex] = item;
            }
            return sortedArr;
        }

        static List<int> SelectionSort()
        {
            List<int> sortedArr = new List<int>(_arr);
            for (int i = 0; i < sortedArr.Count; i++)
            {
                int min = i;
                for (int j = i + 1; j < sortedArr.Count; j++)
                {
                    if (sortedArr[min] > sortedArr[j])
                    {
                        min = j;
                    }
                }

                if (min != i)
                {
                    int lowerValue = sortedArr[min];
                    sortedArr[min] = sortedArr[i];
                    sortedArr[i] = lowerValue;
                }
            }

            return sortedArr;
        }
    }
}
